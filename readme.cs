Yet another ISO Latin-2 Type1 fonts
-----------------------------------
verze 0.9, 28. dubna 2000,
Mat�j Cepl <cepl@bigfoot.com>
Pro podm�nky za, kter�ch je mo�n� pou��vat fonty v t�to
distribuci, viz License.txt (pouze anglicky -- je to GNU/GPL
licence)
==================================================================

Proto�e m� m�ch� zoufal� stav, ve kter�m se nach�zej� voln� Type1 fonty
pro tisknut� dokument� ve v�chodoevropsk�ch jazyc�ch (a proto�e jsem
pot�eboval n�jak� funk�n�), p�ed�lal jsem fonty z bal��ku
XFree86-ISO8859-2-Type1-fonts-1.0-9, kter� je sou��st� m�ho RedHatu
6.0CZ.

Sna�il jsem se co nejm�n� u�kodit (tak aby, doufejme, v�sledn� fonty
byly pou�iteln� pro jak�hokoli V�chod��ra). Mus�m se p�iznat, �e m�
docela �tve stav, ve kter�m se nach�zej� free Type1 fonty pro
v�chodoevropsk� jazyky (sna��m se rozhochodit Lout a ten je podstatn�
vyb�rav�j�� co do shody font� s PostScriptov�mi standardy ne�li TeX).
V�echny fonty, kter� jsem nalezl byly postaveny jenom pro jazyk pana
autora a na v�echny ostatn� se nedost�valo. Na�el jsem polsk� fonty,
kter� neumo��ovaly tisknout �esky nebo ma�arsky, �esk� fonty, kter� zase
neumo��uj� tisk v �e�tin�, a tak. Je mi jasn�, �e je to ��ste�n� d�no
standardem ISO 8859-2, kter� neobsahuje spoustu pot�ebn�ch znak�, ale
p�ece jenom si mysl�m, �e n�jak� pou�iteln�j�� �e�en� by m�lo b�t mo�n�.
Moc tomu nerozum�m, a proto mohu b�t velmi mimo, ale p�ece jenom si
mysl�m, �e mnou navr�en� sestava je rozumn�m kompromisem. Samoz�ejm� r�d
uv�t�m pokud mi n�kdo znalej�� v�ci ne� j� vysv�tl�, pro� jsem vytvo�il
paskvil, ale mus�m se p�iznat, �e pro m� to te� funguje a od dan�ho
�e�en� ne��d�m o mnoho v�ce.

Tak�e, co jsem vlastn� vyvedl?
------------------------------

* zm�nil jsem jm�na glyf�, kter� kolidovali s Adobe fonty (nap�.
  fullstop jsem zm�nil na period)

* p�idal jsem glyphy pro quotedblbase, ellipsis, quotedblright,
  quotedblleft, endash, emdash a bullet (v�echny jsou okop�rovan�
  z voln� ���en�ch font� URW ���en�ch jako sou��st GhostScript-6.01;
  bohu�el, kv�li t�mhle n�kolika glyph�m mus�m ���it v�sledek pod
  GNU/GPL, i kdy� bych osobn� dal p�ednost X licenci).

* p�idal jsem kerningov� p�ry vygenerovan� programem a2ac

Co zb�v� ud�lat?
----------------

* opravit v�echny pitomosti, kter� jsem svoji neznalost�
  zp�sobil :-)

* naj�t n�jak�ho opravodv�ho znalce PostScriptu, aby se mi na
  ty fonty kouknul a opravil mi je (pokud se V�s to t�k�, mohl
  byste se mi ozvat, pros�m?)

* ud�lat n�co (snad s kerningama?), aby fonty dr�ely linku
  (v PDF je to mnohem lep��)

* opravit chov�n� quoteleft

* p�idat ligatury /fi a /fl

* p�idat n�kter� dal�� glyphy (florin, dagger, daggerdbl,
  fraction)

* p�idat k cel� distribuci PGP podpis (porad�te mi n�kdo, jak na to?)

* vybudovat .rpm (jak se d�l� noarch bal��ek?)

Pod�kov�n�
----------

D�kuji zejm�na panu Petru Ol��kovi, kter� mimo jin� sv� kladn�
vlastnosti vytvo�il programy a2ac a t1accent, a kter� mi
odpov�dal na moje p�ipitom�l� dotazy, i kdy� na to zrovna nem�l
�as. Krom� toho, �e kerningov� p�ry jsou vygenerov�ny pomoc�
a2ac, je k ob�ma program�m p�ilo�ena monument�ln� dokumentace,
kter� m� nau�ila v�emu co um�m o Type1 fontech. Pokud se
pot�ebujete dozv�d�t n�co v�ce o Type1 fontech v�ele doporu�uji
st�hnout oba programy (jsou sou��st� RedHatu 6.0CZ anebo p��mo na
str�nce autora http://math.feld.cvut.cz/olsak/ ) a celou
dokumentaci podrobn� prostudovat. Samoz�ejm�, �e d�kuji i autor�m
t1utils, bez kter�ch bych se prost� nehnul.

Nejv�t�� d�ky ale pat�� m�mu P�nu Je���i Kristu, bez jeho�
l�sky, sp�sy a ka�dodenn� p��e, nemohu nic u�initi (Jan 15.5).
