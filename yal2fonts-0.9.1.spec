%define pkgdir yal2f
%define instdir /usr/share/fonts/ISO8859-2/Type1L2

Summary: Free basic PostScript(TM) fonts with extended ISO8859-2 character set
Name: yal2fonts
Version: 0.9.1
Release: 1
Source: http://www.volny.cz/cepls/ps-pdf/yal2f-0.9.1.tar.gz
License: GPL
Group: Applications/Publishing
URL: http://www.volny.cz/cepls/ps-pdf
BuildRoot: /var/tmp/%{name}-%{version}-root
BuildArchitectures: noarch
#BuildPrereq: mkfontdir
#BuildPrereq: enscript

%description 
Free basic PostScript(TM) fonts with extended ISO8859-2 character set

%prep

%setup -q -n %{pkgdir}-%{version}

%build

%install
rm -rf $RPM_BUILD_ROOT
mkdir -p $RPM_BUILD_ROOT%{instdir}

for fontfile in {courier,helvetica,times}/*.{afm,pfb}; do
  install -m 644 $fontfile $RPM_BUILD_ROOT%{instdir}
done

for cfgfile in Fontmap fonts.scale fonts.dir font.map; do
  install -m 644 $cfgfile $RPM_BUILD_ROOT%{instdir}
done

mkdir -p $RPM_BUILD_ROOT/usr/doc/%{name}-%{version}
for docfile in license.txt readme.cs readme.txt test-il2.lt test-il2.ps.gz ivanyi.txt
do
  install -m 644 $docfile $RPM_BUILD_ROOT/usr/doc/%{name}-%{version}
done

# if you want to regenerate cfg files
# (you need mkfontdir and enscript installed)
#
# (cd $RPM_BUILD_ROOT%{instdir}
# mkfontdir)
# (cd $RPM_BUILD_ROOT%{instdir}
# mkafmmap *.afm)

%post
if [ -x /usr/sbin/chkfontpath ]; then
  /usr/sbin/chkfontpath -q -a %{instdir}
  if [ -x /etc/rc.d/init.d/xfs ] && \
      ps ax | grep -v grep | egrep -q '[ ]xfs[ ]' ; then
    /etc/rc.d/init.d/xfs stop
    /etc/rc.d/init.d/xfs start
  fi
fi

%postun
if [ "$1" = "0" ]; then
  if [ -x /usr/sbin/chkfontpath ]; then
    /usr/sbin/chkfontpath -q -r %{instdir}
    if [ -x /etc/rc.d/init.d/xfs ] && \
        ps ax | grep -v grep | egrep -q '[ ]xfs[ ]' ; then
      /etc/rc.d/init.d/xfs stop
      /etc/rc.d/init.d/xfs start
    fi
  fi
fi

%clean
rm -rf $RPM_BUILD_ROOT

%files
%defattr(0644,root,root,0755)
%dir %{instdir}
%config %{instdir}/font*
%config(missingok) %{instdir}/Fontmap
%{instdir}/*.afm
%{instdir}/*.pfb
%doc /usr/doc/%{name}-%{version}/*
